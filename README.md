# ScratchOps

ScratchOps is a set of bash scripts designed to build Linux From Scratch and beyond. This max be (or get) biased towards the Cybernux GNU/Linux distribution.

These scripts will beginn as a copy of the "latest" [LFScript](https://lfscript.org). These were downloaded on 7 Jun 2020, the latest version is revision-20170513. That's over 3 years ago, and a lot does not work well anymore. So I decided to fork it to use for building the Cybernux Operating System, modding anything needed and throwing out unused stuff.


## License
MIT see [LICENSE](LICENSE) for details.


## Documentation
Keep an eye on the [WIKI](../../wikis/home), there will eventually be somme there.


## Bugs, Milestones, Wishes and more
Take a look at the [Issues Center](../../issues).
